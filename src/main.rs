use ansi_term::Colour;
use ansi_term::Style;
use log::{debug, info, trace};
use regex::Regex;
use std::env::{self, Args};
use std::io;

static DEFAULT_COL_ENV_VAR: &str = "RCOL_DEFAULT_COLOR";

struct RegexColorTuple {
    regex: Regex,
    color: Style,
}

// To avoid issues when matches are nested or overlapping, the tool finds all starts and ends, sorts them and then in order prints parts
#[derive(Debug, PartialEq)]
struct StyleStartEvent {
    char: usize,
    style: Style,
}
#[derive(Debug)]
struct StyleEndEvent {
    char: usize,
    style: Style,
}
#[derive(Debug)]
enum TextEvent {
    StyleStart(StyleStartEvent),
    StyleEnd(StyleEndEvent),
}

impl TextEvent {
    fn pos(&self) -> usize {
        match self {
            TextEvent::StyleStart(e) => e.char,
            TextEvent::StyleEnd(e) => e.char,
        }
    }
    fn style(&self) -> Style {
        match self {
            TextEvent::StyleStart(e) => e.style,
            TextEvent::StyleEnd(e) => e.style,
        }
    }
}

type TextEvents = Vec<TextEvent>;

fn main() {
    env_logger::init();
    let cols = if let Some(cols) = parse_args(env::args()) {
        cols
    } else {
        return;
    };
    let stdin = io::stdin();

    for line in stdin.lines() {
        if let Ok(line) = line {
            let mut text_events = find_matches_in_line(&cols, &line);
            print_colorized_line(&mut text_events, &line)
        } else {
            info!("Failed to read stdin: {line:?}");
            return;
        }
        info!("Input EOF");
    }
}

fn parse_flag<S>(arg: S) -> bool
where
    S: AsRef<str> + std::fmt::Display,
{
    match arg.as_ref() {
        "-h" => {
            print_help();
            return false;
        }
        "--help" => {
            print_help();
            return false;
        }
        _ => {
            println!("Unknown argument {arg}");
            print_help();
            return false;
        }
    }
}

fn parse_args(args: Args) -> Option<Vec<RegexColorTuple>> {
    let args = args.skip(1);
    let mut col_tuples = Vec::<RegexColorTuple>::new();
    for argument in args {
        if argument.starts_with("-") {
            if parse_flag(&argument) {
                continue;
            } else {
                return None;
            }
        }
        //let re = Regex::new(r"(?m)^([^:]+):([0-9]+):(.+)$").unwrap();
        debug!("Arg: {argument}");
        if let Some((a, b)) = argument.rsplit_once("=") {
            info!("Using \"{a}\" as regex and \"{b}\" as color");

            let re = Regex::new(a).unwrap();
            let col = if let Some(col) = str_to_col(b, || {
                eprintln!("Unknown color {b}");
                return None;
            }) {
                col
            } else {
                return None;
            };
            col_tuples.push(RegexColorTuple {
                regex: re,
                color: col.into(),
            })
        } else {
            info!("No color provided, using red and \"{argument}\" as regex");
            let re = Regex::new(&argument).unwrap();
            let col = get_default_color();

            col_tuples.push(RegexColorTuple {
                regex: re,
                color: col.into(),
            })
            //println!("Invalid argument {argument}")
        }
    }
    return Some(col_tuples);
}

fn find_matches_in_line(patterns: &Vec<RegexColorTuple>, line: &String) -> TextEvents {
    let mut text_events = TextEvents::new();
    for col in patterns {
        if let Some(occur) = col.regex.find(&line) {
            text_events.push(TextEvent::StyleStart(StyleStartEvent {
                char: occur.start(),
                style: col.color,
            }));
            text_events.push(TextEvent::StyleEnd(StyleEndEvent {
                char: occur.end(),
                style: col.color,
            }));
        }
    }
    text_events.sort_by(|a, b| {
        //let a_pos = if let TextEvent::StyleStart(e) = a{
        //	e.char
        //}else if let TextEvent::StyleEnd(e) = a{
        //	e.char
        //}else{panic!()};
        //let b_pos = if let TextEvent::StyleStart(e) = b{
        //	e.char
        //}else if let TextEvent::StyleEnd(e) = b{
        //	e.char
        //}else{panic!()};
        //a_pos.cmp(&b_pos)
        a.pos().cmp(&b.pos())
    });

    return text_events;
}

fn print_colorized_line(text_events: &mut TextEvents, line: &String) {
    debug!("Input text: \"{line}\"");
    debug!("Formating events: {text_events:?}");

    // No matches on this line.
    if text_events.len() == 0 {
        println!("{line}");
        return;
    }
    let first = text_events[0].pos();
    print!("{}", &line[..first]);
    let reset = ansi_term::Style::default();
    let mut stack = vec![reset];
    loop {
        if text_events.len() > 1 {
            let start = text_events[0].pos();
            let end = text_events[1].pos();
            match &text_events[0] {
                TextEvent::StyleStart(e) => {
                    stack.push(e.style);
                    print!("{}", e.style.paint(&line[start..end]));
                }
                TextEvent::StyleEnd(e) => {
                    let idx = stack.iter().position(|x| *x == e.style).unwrap();
                    stack.remove(idx);
                    print!("{}", stack.last().unwrap().paint(&line[start..end]));
                }
            }
        } else {
            break;
        }
        text_events.remove(0);
    }
    let last = text_events[0].pos();
    println!("{}", &line[last..]);
}

fn str_to_col<F, S>(col: S, invalid_col_handler: F) -> Option<Colour>
where
    F: Fn() -> Option<Colour>,
    S: AsRef<str> + std::fmt::Debug,
{
    trace!("converting {col:?} into color");
    match col.as_ref().to_lowercase().as_str() {
        "black" => Some(Colour::Black),
        "red" => Some(Colour::Red),
        "green" => Some(Colour::Green),
        "yellow" => Some(Colour::Yellow),
        "blue" => Some(Colour::Blue),
        "purple" => Some(Colour::Purple),
        "cyan" => Some(Colour::Cyan),
        "white" => Some(Colour::White),
        "" => Some(Colour::Red),
        "r" => Some(Colour::Red),
        "b" => Some(Colour::Blue),
        "g" => Some(Colour::Green),
        "y" => Some(Colour::Yellow),
        "p" => Some(Colour::Purple),
        "c" => Some(Colour::Cyan),
        "w" => Some(Colour::White),
        _ => invalid_col_handler(),
    }
}

fn get_default_color() -> Style {
    return match env::var(DEFAULT_COL_ENV_VAR) {
        Ok(val) => {
            if let Some(col) = str_to_col(&val, || {
                eprintln!("Invalid color set in env variable({DEFAULT_COL_ENV_VAR}): {val}");
                Some(Colour::Red)
            }) {
                col
            } else {
                Colour::Red
            }
        }
        Err(_) => Colour::Red,
    }
    .into();
}

fn print_help() {
    let exec = env::args().nth(0).unwrap();
    eprintln!("Colorize input matching regex");
    eprintln!("");
    eprintln!("Usage: {exec} ARG *");
    eprintln!("\tARG := PATTERN | FLAG");
    eprintln!("\tFLAG := -h | --help");
    eprintln!("\tPATTERN := REGEX[=COLOR]");
    eprintln!(
        "\tCOLOR := black | r[ed] | g[reen] | y[ellow] | b[lue] | p[urple] | c[yan] | w[hite]"
    );
}

# rcol - Regex Colorizer

`rcol` is a tool that allows you to highlight (change color of) parts of text that match regex.

## Usage

For now, `rcol` only supports `-h` and `--help` flag, which print very basic help screen.

`rcol` accepts multiple patterns as argument.
pattern is in format `REGEX=COLOR`, text matching regex, will have color changed to `COLOR`.
For example

``` sh
uname -a | rcol Linux=Blue
```

Will change the first occurrence of word "Linux" to blue (I'll probably add global and case insensitive flag later).
You can shorten most colors to first letter (e.g. "r" instead of "red"), you can find all colors in help text.

Colors are case insensitive.

### Default colors

You can omit the color part, for example

``` sh
uname -a | rcol Linux
```

Will color the word in red.
You can change the default color using `RCOL_DEFAULT_COLOR` environment variable.

``` sh
export RCOL_DEFAULT_COLOR=blue
uname -a | rcol Linux
```


But if the regex itself contains `=`, you need to specify the color.

``` sh
cat index.html | rcol 'class=idk=red'
```

This is because the last `=` (if present) is interpreted as separator.

## Multiple patterns

You can specify more that one pattern and you can use different colors.
For example:

``` sh
uname -a | rcol "Linux [^ ]+ [^ ]+=Red" "\d+\.\d+\.\d+=yellow" "`hostname`=blue"
```

## Installation

I stil haven't uploaded it as crate, so to install, you can do something like

``` sh
cd $(mktemp -d)
git clone --depth 1 https://gitlab.com/matej-vrba/rcol .
cargo build --release
cp target/release/rcol $HOME/.local/bin
# or
sudo cp target/release/rcol /usr/local/bin
```
